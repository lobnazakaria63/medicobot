import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Patient } from './interface';
import * as $ from "jquery";
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable()
export class MedicobotServiceService {
countryApi='https://medicoapitask.herokuapp.com/getCountries';
cityApi="https://medicoapitask.herokuapp.com/getCities/";
patientApi="https://medicoapitask.herokuapp.com/submitPatient"
  constructor(private http:HttpClient) { }
  getCountry(){
   
    return this.http.get(this.countryApi,httpOptions)
  }
 getCity(id:number){
   
  let httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    param: id
  };
   return this.http.get(this.cityApi+id,httpOptions)
 }

 postData(body:Patient){
   debugger
   let b =JSON.stringify(body);
   return this.http.post(this.patientApi,b,httpOptions)
 }
}
