import { TestBed, inject } from '@angular/core/testing';

import { MedicobotServiceService } from './medicobot-service.service';

describe('MedicobotServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MedicobotServiceService]
    });
  });

  it('should be created', inject([MedicobotServiceService], (service: MedicobotServiceService) => {
    expect(service).toBeTruthy();
  }));
});
