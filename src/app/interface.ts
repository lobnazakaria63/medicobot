export interface Patient{
    name:string;
    country_id:number;
    city_id:number;
    date_of_birth:Date;
    gender:string;
    male_question?:string;
    female_question?:string;
}
export interface Countries{
    id:number;
    name:string;
}
export interface Cities{
    id:number;
    country_id:number;
    name:string;
}