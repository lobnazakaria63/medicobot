import { Component, OnInit} from '@angular/core';
import { FormGroup, FormControl,FormBuilder } from '@angular/forms';
import { MedicobotServiceService } from './medicobot-service.service';
import { Countries, Cities } from './interface';
import { Validators } from '@angular/forms';
import * as $ from 'jquery';
import { DatePipe } from '@angular/common';
import { Response } from '@angular/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'app';
  countryArr:any=[];
  cityArr:any=[]; 
  date:any;
  cityId:any;
  selectedCountries:any;
  gender:any;
  constructor(private medicobotService:MedicobotServiceService,
    private datePipe: DatePipe,
    private fb: FormBuilder
  ){}


  patientForm = this.fb.group({
    name:['',Validators.required],
    country_id:['',Validators.required],
    city_id:['',Validators.required],
    date_of_birth:['',Validators.required],
    gender:['',Validators.required],
    male_question:[''],
    female_question:[''],
  }); 


ngOnInit(){
  //listing array of cities in **countryArr** Array container to use it later
  this.medicobotService.getCountry().subscribe(data=>{
    var dataArr= data as Countries[];
    for (var i=0 ; i< dataArr.length; i++){
      this.countryArr[i]=dataArr[i];
    }
    console.log("countries= ", this.countryArr);
    })
 }
// .......... selecting a country and trigger the city selection dropdown list
  onSelect(id:any){
    this.medicobotService.getCity(id).subscribe(data => {
      var dataArr=data as Cities[];
     for (var i=0 ; i< dataArr.length; i++){
       this.cityArr[i]=dataArr[i];
     }
      
    })
  }
//.......chooseing whether the gender is male or female and make the question appear 
  displayQues(){
    console.log("gender is = ",this.gender)
    if (this.gender == "male"){
    $("#male").css("display"," block");
    $("#female").css("display","none");
    }
    else{
      $("#female").css("display"," block");
      $("#male").css("display","none");
    }
    
  }

//submit the data of the form to the url api ..
onSubmit(){
  debugger
  this.date = this.datePipe.transform(this.patientForm.controls['date_of_birth'].value, 'yyyy-MM-dd');
  this.patientForm.controls['country_id'].setValue(this.selectedCountries);
  this.patientForm.controls['city_id'].setValue(this.cityId);
  this.patientForm.controls['date_of_birth'].setValue(this.date)
 var formValues= this.patientForm.value;
 this.medicobotService.postData(formValues).subscribe(data => 
  {
    console.log(data);
  },
  err => {
    console.log(err);
  });
 

}

}
